FROM node:14

LABEL maintainer="arcila@ebi.ac.uk"

ENV APP_SOURCE /usr/src/unichem

RUN mkdir -p $APP_SOURCE
WORKDIR $APP_SOURCE

RUN apt-get update -qq -y && \
    apt-get upgrade -qq -y && \
    apt-get install -qq -y git

COPY . $APP_SOURCE

ARG UNICHEM_API_URL
ENV UNICHEM_API_URL $UNICHEM_API_URL

RUN npm install && npm run build

ENV HOST 0.0.0.0

EXPOSE 3000

ENTRYPOINT [ "npm", "start" ]
