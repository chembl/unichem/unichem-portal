import axios from 'axios'

export default class RestAPI {
  getUnichemAPIURL() {
    return process.env.unichemApiUrl + '/v1'
  }

  getUnichemAPI() {
    // const path = `${process.env.VUE_APP_ROOT_API}/${process.env.VUE_APP_SERVER_BASE_PATH}glados_api/chembl/unichem`
    // const path = `https://wwwdev.ebi.ac.uk/unichem_beta/back/api/v1/compounds/compound`
    const path = this.getUnichemAPIURL()
    return axios.create({
      baseURL: path,
      withCredentials: false,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    })
  }

  getBeakerAPI() {
    const path = 'https://www.ebi.ac.uk/chembl/api/utils'
    return axios.create({
      baseURL: path,
      withCredentials: false
    })
  }
}
