module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: '@babel/eslint-parser',
    requireConfigFile: false,
    babelOptions: { configFile: './.babelrc' }
  },
  extends: [
    '@nuxtjs',
    'plugin:nuxt/recommended',
    'plugin:vue/recommended',
    'eslint:recommended'
    // 'prettier',
    // 'plugin:prettier/recommended',
  ],
  // add your custom rules here
  rules: {
    'vue/multi-word-component-names': ['error', {
      ignores: ['default']
    }],
    'vue/component-name-in-template-casing': ['error', 'PascalCase'],
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'import/no-unresolved': 0,
    'import/no-unassigned-import': 0,
    semi: ['error', 'never'],
    'space-before-function-paren': [
      'error',
      {
        anonymous: 'never',
        named: 'never',
        asyncArrow: 'always'
      }
    ]
  },
  globals: {
    $nuxt: true
  },
  plugins: [
    'prettier'
  ]
}
