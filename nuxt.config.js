export default {
  router: {
    base: '/unichem'
  },
  env: {
    baseUrl: process.env.BASE_URL || 'http://localhost:3000',
    unichemApiUrl: process.env.UNICHEM_API_URL || 'https://www.ebi.ac.uk/unichem/api'
  },
  /*
   ** Headers of the page
   */
  head: {
    titleTemplate: '%s',
    title: 'UniChem',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=product+sans|Roboto:300,400,500,700|Material+Icons'
      }
    ]
  },
  // googleAnalytics: {
  //   id: 'G-69532RH9CG'
  // },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: '@plugins/localStorage.js', ssr: false },
    { src: '@plugins/vuetify.js' },
    { src: '~/plugins/ganalytics.js', mode: 'client' },
    { src: '@plugins/apexcharts.js', ssr: false }
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/vuetify'
    // '@nuxtjs/google-analytics'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/style-resources'
  ],
  styleResources: {
    scss: ['assets/scss/main.scss']
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    customVariables: ['~/assets/vuetify/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: '#00BFA5',
          accent: '#09979B',
          secondary: '#75D8D5',
          success: '#4CAF50',
          info: '#2196F3',
          warning: '#FB8C00',
          error: '#FF5252'
        },
        light: {
          primary: '#09979B',
          accent: '#00BFA5',
          secondary: '#0D595F',
          success: '#4CAF50',
          info: '#2196F3',
          warning: '#FB8C00',
          error: '#FF5252'
        }
      }
    }
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  }
}
