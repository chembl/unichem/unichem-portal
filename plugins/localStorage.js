import createPersistedState from 'vuex-persistedstate'

export default ({ store }) => {
  window.onNuxtReady(() => {
    createPersistedState({
      key: 'unichemVuex',
      // modules: ['SourcesList', 'ConnectivitySearch', 'CompoundSources']
      reducer(val) {
        if (val.general.reset === true) {
          return {}
        }
        return val
      }
    })(store)
  })
}
