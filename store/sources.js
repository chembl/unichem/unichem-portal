import RestAPI from '@/api/glados'

const unichemAPI = new RestAPI()

export const state = () => ({
  list: [],
  loading: true,
  gotSources: false,
  error: {},
  expiryDate: ''
})

export const mutations = {
  remove(state, { todo }) {
    state.list.splice(state.list.indexOf(todo), 1)
  },
  toggle(state, todo) {
    todo.done = !todo.done
  },
  SET_LOADING(state, flag) {
    state.loading = flag
  },
  SET_SOURCES(state, response) {
    const sources = response.sources
    sources.sort((a, b) => a.sourceID - b.sourceID)
    state.list = sources
    if (state.list.length > 0) {
      state.gotSources = true
    }
  },
  SET_ERROR(state, error) {
    state.error = error
  },
  RESET_STATE(state) {
    state.list = []
    state.total = 0
    state.gotSources = false
  },
  SET_EXPIRY_DATE: (state) => {
    const date = new Date()
    // date.setSeconds(date.getSeconds() + 5)
    date.setHours(date.getHours() + 2)
    state.expiryDate = date
  }
}

export const actions = {
  async get({ commit }) {
    commit('SET_LOADING', true)
    commit('RESET_STATE')
    commit('SET_ERROR', {})
    await unichemAPI
      .getUnichemAPI()
      .get('/sources')
      .then((res) => {
        if (res.status === 200) {
          commit('SET_SOURCES', res.data)
          commit('SET_EXPIRY_DATE')
        } else {
          commit('SET_ERROR', new Error(res))
        }
        commit('SET_LOADING', false)
      })
      .catch((error) => {
        console.log(error)
        commit('SET_ERROR', error)
        commit('SET_LOADING', false)
      })
  }
}
