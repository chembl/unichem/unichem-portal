import RestAPI from '@/api/glados'

const unichemAPI = new RestAPI()

export const state = () => ({
  sources: [],
  total: 0,
  loading: false,
  error: {},
  currentCompound: {},
  isSuccessful: false,
  expiryDate: '',
  retrievedCompounds: []
})

export const mutations = {
  RESET_STATE(state) {
    state.sources = []
    state.total = 0
    state.loading = false
    state.error = {}
    state.currentCompound = {}
    state.isSuccessful = false
    state.expiryDate = ''
  },
  remove(state, { todo }) {
    state.list.splice(state.list.indexOf(todo), 1)
  },
  SET_ERROR(state, error) {
    state.error = error
  },
  SET_SUCCESS(state, status) {
    state.isSuccessful = status
  },
  toggle(state, todo) {
    todo.done = !todo.done
  },
  SET_LOADING(state, flag) {
    state.loading = flag
  },
  SET_COMPOUNDS(state, response) {
    state.total = 0
    state.currentCompound = {}
    state.sources = []
    const compounds = response.compounds
    state.retrievedCompounds = compounds
    state.currentCompound = compounds[0]
    compounds.forEach((comp) => {
      comp.sources.forEach((source) => {
        source.uniqueID = source.compoundId + source.shortName + comp.uci
        const sC = {
          uci: comp.uci,
          inchi: comp.inchi.inchi
        }
        source.searchedCompound = sC

        state.sources.push(source)
      })
      comp.sources = []
    })
    state.currentCompound.sources = state.currentCompound.sources.map((source) => {
      source.uniqueID = source.compoundId + source.shortName
      return source
    })
    if (compounds.length > 0) {
      state.isSuccessful = true
    }
    state.total = parseInt(response.totalCompounds)
  },
  SET_EXPIRY_DATE: (state) => {
    const date = new Date()
    // date.setSeconds(date.getSeconds() + 30)
    date.setHours(date.getHours() + 6)
    state.expiryDate = date
  }
}

export const actions = {
  async get({ commit }, query) {
    commit('SET_LOADING', true)
    commit('SET_ERROR', {})
    commit('SET_SUCCESS', false)

    const params = {
      start: query.start,
      limit: query.limit
    }

    await unichemAPI
      .getUnichemAPI()
      .post('/compounds', query.payload, { params })
      .then((res) => {
        if (res.status === 200) {
          if (res.data.response === 'Not found') {
            commit('SET_ERROR', new Error('No compounds found'))
          } else {
            commit('SET_COMPOUNDS', res.data)
            commit('SET_EXPIRY_DATE')
          }
        } else {
          commit('SET_ERROR', new Error(res))
        }
        commit('SET_LOADING', false)
      })
      .catch((error) => {
        console.log(error)
        commit('SET_ERROR', error)
        commit('SET_LOADING', false)
      })
  }
}
