import RestAPI from '@/api/glados'

const unichemAPI = new RestAPI()

export const state = () => ({
  list: [],
  total: 0,
  searchedCompound: {},
  currentPage: 1,
  loading: false,
  error: {},
  expiryDate: '',
  isSuccessful: false
})

export const mutations = {
  add(state, text) {
    state.list.push({
      text,
      done: false
    })
  },
  remove(state, { todo }) {
    state.list.splice(state.list.indexOf(todo), 1)
  },
  toggle(state, todo) {
    todo.done = !todo.done
  },
  SET_LOADING(state, flag) {
    state.loading = flag
  },
  SET_SUCCESS(state, status) {
    state.isSuccessful = status
  },
  SET_CURRENT_PAGE(state, page) {
    state.currentPage = page
  },
  SET_CONNECTIVITY_SOURCES(state, connectivitySources) {
    const sources = connectivitySources.sources
    state.list = sources.map((source) => {
      source.uniqueID = source.compoundId + source.shortName
      return source
    })
    if (connectivitySources.sources.length > 0) {
      state.isSuccessful = true
    }
    state.total = parseInt(connectivitySources.totalSources)
    state.searchedCompound = connectivitySources.searchedCompound
  },
  SET_ERROR(state, error) {
    state.error = error
  },
  RESET_STATE(state) {
    state.list = []
    state.total = 0
    state.searchedCompound = {}
    state.currentPage = 1
    state.error = {}
    state.expiryDate = ''
    state.isSuccessful = false
  },
  SET_EXPIRY_DATE: (state) => {
    const date = new Date()
    // date.setSeconds(date.getSeconds() + 10)
    date.setHours(date.getHours() + 6)
    state.expiryDate = date
  }
}

export const actions = {
  async get({ commit }, query) {
    commit('SET_LOADING', true)
    commit('SET_SUCCESS', false)

    const params = {
      start: query.start,
      limit: query.limit
    }

    await unichemAPI
      .getUnichemAPI()
      .post('/connectivity', query.payload, { params })
      .then((res) => {
        if (res.status === 200) {
          if (res.data.response === 'Not found') {
            commit('SET_ERROR', new Error('No compounds found'))
          } else {
            commit('SET_CONNECTIVITY_SOURCES', res.data)
            commit('SET_EXPIRY_DATE')
          }
        } else {
          commit('SET_ERROR', new Error(res))
        }
        commit('SET_LOADING', false)
      })
      .catch((error) => {
        // ToDo: Handle not found error properly
        console.log('WHEN ERROR ', error)
        commit('SET_LOADING', false)
        commit('SET_ERROR', error)
      })
  }
}
