export const state = () => ({
  counter: 0,
  expiryDate: '',
  reset: false,
  acceptedPrivacyNotice: false
})

export const mutations = {
  increment(state) {
    state.counter++
  },
  SET_EXPIRY_DATE: (state) => {
    const date = new Date()
    // date.setSeconds(date.getSeconds() + 20)
    date.setDate(date.getDate() + 3)
    state.expiryDate = date
    state.reset = false
  },
  RESET: (state) => {
    state.reset = true
  },
  RESTART: (state) => {
    state.reset = false
  },
  MODIFY_PRIVACY_NOTICE: (state, pns) => { state.acceptedPrivacyNotice = pns }
}
