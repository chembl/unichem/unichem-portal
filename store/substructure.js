import RestAPI from '@/api/glados'

const unichemAPI = new RestAPI()

export const state = () => ({
  list: [],
  total: 0,
  currentPage: 1,
  loading: true,
  molecule: ''
})

export const mutations = {
  addSubstructure(state, text) {
    state.list.push({
      text,
      done: false
    })
  },
  removeSubstructure(state, { todo }) {
    state.list.splice(state.list.indexOf(todo), 1)
  },
  toggleSubstructure(state, todo) {
    todo.done = !todo.done
  },
  SET_LOADING_SUBSTRUCTURE(state, flag) {
    state.loading = flag
  },
  SET_CURRENT_PAGE_SUBSTRUCTURE(state, page) {
    state.currentPage = page
  },
  SET_SUBSTRUCTURE_COMPOUNDS_SUBSTRUCTURE(state, similarCompounds) {
    const compounds = similarCompounds.compounds
    compounds.map(compound => {
      compound.show = true
      return compound
    })
    state.list = compounds
    state.total = parseInt(similarCompounds.totalCompounds)
  },
  SET_SEARCHED_MOLECULE_SUBSTRUCTURE(state, molecule) {
    state.molecule = molecule
  },
  SET_SEARCHED_THRESHOLD_SUBSTRUCTURE(state, threshold) {
    state.threshold = threshold
  }
}

export const actions = {
  async getSubstructure({ commit }, query) {
    commit('SET_LOADING', true)

    const params = {
      start: query.start,
      limit: query.limit
    }

    await unichemAPI
      .getUnichemAPI()
      .post('/substructure', query.payload, { params })
      .then(res => {
        if (res.status === 200) {
          commit('SET_SUBSTRUCTURE_COMPOUNDS_SUBSTRUCTURE', res.data)
          commit('SET_SEARCHED_MOLECULE_SUBSTRUCTURE', query.payload.compound)
          commit('SET_SEARCHED_THRESHOLD_SUBSTRUCTURE', params.threshold)
        }
        commit('SET_LOADING_SUBSTRUCTURE', false)
      })
      .catch(error => {
        console.log(error)
        commit('SET_LOADING_SUBSTRUCTURE', false)
      })
  }
}
