import RestAPI from '@/api/glados'

const unichemAPI = new RestAPI()

export const state = () => ({
  similarityList: [],
  similarityTotal: 0,
  similarityCurrentPage: 1,
  similarityLoading: false,
  similarityMolecule: '',
  similarityThreshold: 0.80,
  substructureList: [],
  substructureTotal: 0,
  substructureCurrentPage: 1,
  substructureLoading: false,
  substructureMolecule: '',
  substructureThreshold: 0.93,
  connectivityList: [],
  connectivityTotal: 0,
  connectivityCurrentPage: 1,
  connectivityLoading: true,
  connectivityMolecule: '',
  connectivityThreshold: 0.93,
  gotResponse: false

})

export const mutations = {
  addSimilarity(state, text) {
    state.list.push({
      text,
      done: false
    })
  },
  addSubstructure(state, text) {
    state.list.push({
      text,
      done: false
    })
  },
  removeSimilarity(state, { todo }) {
    state.list.splice(state.list.indexOf(todo), 1)
  },
  removeSubstructure(state, { todo }) {
    state.list.splice(state.list.indexOf(todo), 1)
  },
  toggleSimilarity(state, todo) {
    todo.done = !todo.done
  },
  toggleSubstructure(state, todo) {
    todo.done = !todo.done
  },
  SET_GOT_RESPONSE(state, flag) {
    state.gotResponse = flag
  },
  SET_LOADING_SIMILARITY(state, flag) {
    state.similarityLoading = flag
  },
  SET_LOADING_SUBSTRUCTURE(state, flag) {
    state.loading = flag
  },
  SET_CURRENT_PAGE_SIMILARITY(state, page) {
    state.similarityCurrentPage = page
  },
  SET_SIMILAR_COMPOUNDS_SIMILARITY(state, similarCompounds) {
    const compounds = similarCompounds.compounds
    if (compounds.length > 0) {
      state.gotResponse = true
    }
    compounds.map((compound) => {
      compound.show = true
      compound.size = '4'
      compound.maximized = false
      return compound
    })

    state.similarityList = compounds
    state.similarityTotal = parseInt(similarCompounds.totalCompounds)
  },
  SET_SUBSTRUCTURE_COMPOUNDS_SUBSTRUCTURE(state, similarCompounds) {
    const compounds = similarCompounds.compounds
    compounds.map((compound) => {
      compound.show = true
      return compound
    })
    state.list = compounds
    state.total = parseInt(similarCompounds.totalCompounds)
  },
  SET_SEARCHED_MOLECULE_SIMILARITY(state, molecule) {
    state.similarityMolecule = molecule
  },
  SET_SEARCHED_MOLECULE_SUBSTRUCTURE(state, molecule) {
    state.molecule = molecule
    console.log(state.molecule)
  },
  SET_SEARCHED_THRESHOLD_SIMILARITY(state, threshold) {
    state.threshold = threshold
  },
  SET_SEARCHED_THRESHOLD_SUBSTRUCTURE(state, threshold) {
    state.threshold = threshold
  }
}

export const actions = {
  async getSimilarity({ commit }, query) {
    commit('SET_LOADING_SIMILARITY', true)
    commit('SET_GOT_RESPONSE', false)

    const params = {
      threshold: query.threshold,
      start: query.start,
      limit: query.limit
    }
    console.log(params, 'QUERY PARAMS')

    await unichemAPI
      .getUnichemAPI()
      .post('/similarity', query.payload, { params })
      .then((res) => {
        if (res.status === 200) {
          commit('SET_SIMILAR_COMPOUNDS_SIMILARITY', res.data)
          commit('SET_SEARCHED_MOLECULE_SIMILARITY', query.payload.compound)
          commit('SET_SEARCHED_THRESHOLD_SIMILARITY', params.threshold)
        }
        commit('SET_LOADING_SIMILARITY', false)
      })
      .catch((error) => {
        console.log(error)
        commit('SET_LOADING_SIMILARITY', false)
      })
  },
  async getSubstructure({ commit }, query) {
    commit('SET_LOADING', true)

    const params = {
      start: query.start,
      limit: query.limit
    }

    await unichemAPI
      .getUnichemAPI()
      .post('/substructure', query.payload, { params })
      .then((res) => {
        if (res.status === 200) {
          commit('SET_SUBSTRUCTURE_COMPOUNDS_SUBSTRUCTURE', res.data)
          commit('SET_SEARCHED_MOLECULE_SUBSTRUCTURE', query.payload.compound)
          commit('SET_SEARCHED_THRESHOLD_SUBSTRUCTURE', params.threshold)
        }
        commit('SET_LOADING_SUBSTRUCTURE', false)
      })
      .catch((error) => {
        console.log(error)
        commit('SET_LOADING_SUBSTRUCTURE', false)
      })
  }
}
